import React, { Component } from 'react'
import { View, Text } from 'react-native'
import { Icon, Button } from 'react-native-elements'
import Style from './BottomNavigatorStyle'
import { Colors } from '../Theme'

class BottomNavigator extends Component {
  render() {
    return (
      <View style={Style.container}>
        <View style={Style.buttonContainer}>
          <Button
            icon={{
              name: 'shopping-cart',
              type: 'feather',
              color: '#fff',
              style: { marginRight: 0 },
            }}
            // onPress={() => this.doSomething()}
            buttonStyle={Style.buttonStyle}
            containerViewStyle={Style.containerViewButton}
          />
        </View>
        <View style={Style.containerIcon1}>
          <View style={Style.containerIcon2}>
            <Icon
              name="home"
              type="feather"
              color={Colors.buttonRed}
              // onPress={() => this.doSomething()} // Ex : openDrawer() in react-navigation
            />
            <Text style={Style.textTab}>Home</Text>
          </View>
          <View style={Style.containerIcon2}>
            <Icon name="user" type="feather" color={Colors.grey} />
            <Text style={Style.textTab2}>Profile</Text>
          </View>
        </View>
      </View>
    )
  }
}

export default BottomNavigator

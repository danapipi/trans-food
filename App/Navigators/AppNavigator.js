import { createAppContainer, createStackNavigator } from 'react-navigation'

import MainScreen from 'App/Containers/Dashboard/MainScreen'
import SplashScreen from 'App/Containers/SplashScreen/SplashScreen'
import DetailsScreen from 'App/Containers/Details/DetailsScreen'

const StackNavigator = createStackNavigator(
  {
    SplashScreen,
    MainScreen,
    details: { screen: DetailsScreen },
  },
  {
    initialRouteName: 'SplashScreen',
    headerMode: 'none',
  }
)

export default createAppContainer(StackNavigator)

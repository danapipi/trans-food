/* eslint-disable react/prop-types */
import React from 'react'
import { Text, View, Image } from 'react-native'
import Style from './ListMenuStyle'

const ListMenu = ({ item }) => (
  <View style={Style.container}>
    <View style={Style.textContainer}>
      <Text style={Style.text}>{item.food_name}</Text>
      <Text style={Style.text}>{item.food_price}</Text>
    </View>
    <Image source={item.food_img} style={Style.image} />
  </View>
)

export default ListMenu

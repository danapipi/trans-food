import { StyleSheet } from 'react-native'
// import Fonts from 'App/Theme/Fonts'
import ApplicationStyles from 'App/Theme/ApplicationStyles'
import { Colors } from 'App/Theme'

export default StyleSheet.create({
  container: { ...ApplicationStyles.screen.container, flex: 1 },
  containerBg: { marginBottom: 80, height: 220 },
  headerContainer: { flex: 1 },
  restoContainer: {
    flex: 2,
    elevation: 2,
    borderRadius: 15,
    backgroundColor: Colors.white,
    marginHorizontal: 14,
    top: 40,
  },
  restoName: {
    color: Colors.black,
    fontSize: 26,
    marginHorizontal: 10,
    marginTop: 15,
    marginBottom: 10,
  },
  bgImage: {
    width: '100%',
    zIndex: -10,
    position: 'absolute',
    top: 0,
    resizeMode: 'cover',
    flex: 1,
    borderBottomLeftRadius: 30,
    borderBottomRightRadius: 30,
  },
  imageButton: { flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 },
  back: { marginLeft: 15 },
  like: { marginRight: 10 },
  image: { width: 25, height: 25, top: 5, resizeMode: 'contain' },
  imageKey: {
    width: 12,
    height: 12,
    marginHorizontal: 10,
    resizeMode: 'contain',
    alignSelf: 'center',
  },
  textCuisine: {
    color: Colors.textRed,
    fontSize: 10,
    margin: 5,
  },
  containerCuisine: { flexDirection: 'row', marginHorizontal: 10 },
  cuisine: { backgroundColor: Colors.bgRed, borderRadius: 15, marginHorizontal: 3 },
  textContainer: {
    flexDirection: 'row',
    borderBottomColor: Colors.grey,
    borderBottomWidth: 0.3,
    marginHorizontal: 10,
    marginVertical: 15,
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  text: { fontSize: 14, marginBottom: 10 },
  switch: { marginBottom: 10, marginRight: 10 },
  menuContainer: { flex: 3 },
  titleMenu: {
    fontWeight: 'bold',
    fontSize: 18,
    color: Colors.black,
    marginLeft: 10,
    marginBottom: 5,
  },
  bottomResto: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 10,
    marginTop: 10,
    marginBottom: 15,
  },
  estTime: {
    fontSize: 13,
    color: 'black',
    marginHorizontal: 10,
  },
  imageRating: {
    width: 15,
    height: 15,
    marginHorizontal: 8,
  },
  textRating: {
    color: 'black',
  },
})

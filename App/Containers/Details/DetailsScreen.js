/* eslint-disable react/prop-types */
import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity, Switch } from 'react-native'
import { Images, Colors } from 'App/Theme'
import Style from './DetailsScreenStyle'
import { FlatList } from 'react-native-gesture-handler'
import ListMenu from './ListMenu'
export default class DetailsScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      switch: false,
    }
  }

  changeSwitch = () => {
    this.setState({ switch: !this.state.switch })
  }
  goBack = () => {
    this.props.navigation.goBack()
  }

  render() {
    const { data } = this.props.navigation.state.params
    return (
      <View style={Style.container}>
        <View style={Style.containerBg}>
          <View style={Style.headerContainer}>
            <Image source={data.bgHeader} style={Style.bgImage} />
            <View style={Style.imageButton}>
              <TouchableOpacity style={Style.back} onPress={() => this.goBack()}>
                <Image source={Images.back} style={Style.image} />
              </TouchableOpacity>
              <TouchableOpacity style={Style.like}>
                {data.like ? (
                  <Image source={Images.like1} style={Style.image} />
                ) : (
                  <Image source={Images.like0} style={Style.image} />
                )}
              </TouchableOpacity>
            </View>
          </View>
          <View style={Style.restoContainer}>
            <Text style={Style.restoName}> {data.resto_name} </Text>
            <View style={Style.containerCuisine}>
              {data.cusine.map((text, key) => (
                <View key={key + 1} style={Style.cuisine}>
                  <Text key style={Style.textCuisine}>
                    {text.name}
                  </Text>
                </View>
              ))}
              <Image source={Images.key} style={Style.imageKey} />
            </View>
            <View style={Style.bottomResto}>
              <Text style={Style.estTime}>{data.est_time}</Text>
              <Text style={Style.textRating}>{data.rating}</Text>
              <Image source={Images.star} style={Style.imageRating} />
              {data.user_rating > 500 ? <Text>(500+)</Text> : <Text>(400+)</Text>}
            </View>
          </View>
        </View>
        <View style={Style.textContainer}>
          <Text style={Style.text}>Veg only</Text>
          <View style={Style.switch}>
            <Switch
              onValueChange={() => this.changeSwitch()}
              value={this.state.switch}
              trackColor={Colors.buttonRed}
            />
          </View>
        </View>
        <View style={Style.menuContainer}>
          <Text style={Style.titleMenu}>Picked for you</Text>
          <FlatList
            data={data.menu}
            extraData={data.menu}
            keyExtractor={(item, key) => item + key}
            renderItem={({ item }) => <ListMenu item={item} />}
            showsVerticalScrollIndicator={false}
          />
        </View>
      </View>
    )
  }
}

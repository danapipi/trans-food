import { StyleSheet } from 'react-native'
// import Fonts from 'App/Theme/Fonts'
import ApplicationStyles from 'App/Theme/ApplicationStyles'
// import { Colors } from 'App/Theme'

export default StyleSheet.create({
  container: {
    ...ApplicationStyles.screen.container,
    flex: 1,
    marginHorizontal: 10,
    flexDirection: 'row',
    marginVertical: 15,
  },
  image: {
    width: 70,
    height: 70,
    top: 0,
    right: 0,
    borderRadius: 10,
    flex: 1,
    resizeMode: 'contain',
  },
  textContainer: { flexDirection: 'column', flex: 2, justifyContent: 'center' },
  text: { fontSize: 16, marginVertical: 5 },
})

import { StyleSheet } from 'react-native'
import Fonts from 'App/Theme/Fonts'
// import { Colors } from 'App/Theme'

export default StyleSheet.create({
  container: {
    height: '65%',
  },
  title: {
    ...Fonts.style.medium,
    marginTop: 5,
  },
  image: {
    width: 62,
    height: 62,
  },
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 10,
  },
})

/* eslint-disable no-undef */
/* eslint-disable react/prop-types */
import React from 'react'
import { Text, View, StatusBar, FlatList, Image, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'
import Style from './MainScreenStyle'
import RenderHeader from './RenderHeader'
import ListScreen from './ListScreen'
import { Images } from 'App/Theme'
import BottomNavigator from '../../Components/BottomNavigator'

class MainScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      dataList: [
        {
          id: 0,
          category: 'All',
          img: Images.allCat,
          resto: [
            {
              resto_id: 1,
              resto_name: 'Burger Singh Club - Sector 50',
              est_time: '30 - 40 min',
              rating: '4.3',
              user_rating: 530,
              like: true,
              verify: false,
              bgHeader: Images.bgHeader,
              image: Images.bgAmerican,
              cusine: [{ name: 'Burger' }, { name: 'American' }],
              menu: [
                {
                  food_id: 1,
                  food_name: 'Chicken Keema',
                  food_price: 'Rp 60.000,-',
                  food_img: Images.menu1,
                },
                {
                  food_id: 2,
                  food_name: 'Udta Punjab 2.0 Burger',
                  food_price: 'Rp 69.000,-',
                  food_img: Images.menu2,
                },
                {
                  food_id: 3,
                  food_name: 'Amnitsari Murgh Makhani',
                  food_price: 'Rp 79.000,-',
                  food_img: Images.menu3,
                },
                {
                  food_id: 4,
                  food_name: 'American Grilled',
                  food_price: 'Rp 50.000,-',
                  food_img: Images.menu2,
                },
              ],
            },
            {
              resto_id: 2,
              resto_name: 'Seafood 99 - Sector 50',
              est_time: '30 - 40 min',
              rating: '4.6',
              user_rating: 520,
              like: false,
              verify: true,
              bgHeader: Images.bgHeader,
              image: Images.bgChinese,
              cusine: [{ name: 'Seafood' }, { name: 'Chinese' }],
              menu: [
                {
                  food_id: 1,
                  food_name: 'Kungpow Chicken',
                  food_price: 'Rp 60.000,-',
                  food_img: Images.menu1,
                },
                {
                  food_id: 2,
                  food_name: 'Cap Cay',
                  food_price: 'Rp 69.000,-',
                  food_img: Images.menu2,
                },
                {
                  food_id: 3,
                  food_name: 'Fu Yung Hai',
                  food_price: 'Rp 79.000,-',
                  food_img: Images.menu3,
                },
                {
                  food_id: 4,
                  food_name: 'I fu Mie',
                  food_price: 'Rp 50.000,-',
                  food_img: Images.menu2,
                },
              ],
            },
            {
              resto_id: 3,
              resto_name: `L'arc~en~ciel Club - Sector 50`,
              est_time: '30 - 40 min',
              rating: '4.5',
              user_rating: 530,
              like: false,
              verify: false,
              bgHeader: Images.bgHeader,
              image: Images.bgFrench,
              cusine: [{ name: 'Vegetarian' }, { name: 'French' }],
              menu: [
                {
                  food_id: 1,
                  food_name: 'Spinach with Soy Sauce',
                  food_price: 'Rp 60.000,-',
                  food_img: Images.menu1,
                },
                {
                  food_id: 2,
                  food_name: 'Mix Vegetable with Soy Sauce',
                  food_price: 'Rp 69.000,-',
                  food_img: Images.menu2,
                },
              ],
            },
            {
              resto_id: 4,
              resto_name: 'Singh - Sector 50',
              est_time: '30 - 40 min',
              rating: '4.0',
              user_rating: 430,
              like: false,
              verify: false,
              bgHeader: Images.bgHeader,
              image: Images.bgAmerican,
              cusine: [{ name: 'Rice Delights' }, { name: 'Indian' }],
              menu: [
                {
                  food_id: 1,
                  food_name: 'Butter Chicken',
                  food_price: 'Rp 60.000,-',
                  food_img: Images.menu1,
                },
                {
                  food_id: 2,
                  food_name: 'Lamb Biryani',
                  food_price: 'Rp 69.000,-',
                  food_img: Images.menu2,
                },
                {
                  food_id: 3,
                  food_name: 'Yellow Daal',
                  food_price: 'Rp 79.000,-',
                  food_img: Images.menu3,
                },
              ],
            },
          ],
        },
        {
          id: 1,
          category: 'Chinese',
          img: Images.chinese,
          resto: [
            {
              resto_id: 1,
              resto_name: 'Seafood 99 - Sector 50',
              est_time: '30 - 40 min',
              rating: '4.6',
              user_rating: 520,
              like: false,
              verify: false,
              bgHeader: Images.bgHeader,
              image: Images.bgChinese,
              cusine: [{ name: 'Seafood' }, { name: 'Chinese' }],
              menu: [
                {
                  food_id: 1,
                  food_name: 'Kungpow Chicken',
                  food_price: 'Rp 60.000,-',
                  food_img: Images.menu1,
                },
                {
                  food_id: 2,
                  food_name: 'Cap Cay',
                  food_price: 'Rp 69.000,-',
                  food_img: Images.menu2,
                },
                {
                  food_id: 3,
                  food_name: 'Fu Yung Hai',
                  food_price: 'Rp 79.000,-',
                  food_img: Images.menu3,
                },
                {
                  food_id: 4,
                  food_name: 'I fu Mie',
                  food_price: 'Rp 50.000,-',
                  food_img: Images.menu2,
                },
              ],
            },
          ],
        },
        {
          id: 2,
          category: 'French',
          img: Images.french,
          resto: [
            {
              resto_id: 1,
              resto_name: `L'arc~en~ciel Club - Sector 50`,
              est_time: '30 - 40 min',
              rating: '4.5',
              user_rating: 530,
              like: false,
              verify: false,
              bgHeader: Images.bgHeader,
              image: Images.bgFrench,
              cusine: [{ name: 'Vegetarian' }, { name: 'French' }],
              menu: [
                {
                  food_id: 1,
                  food_name: 'Spinach with Soy Sauce',
                  food_price: 'Rp 60.000,-',
                  food_img: Images.menu1,
                },
                {
                  food_id: 2,
                  food_name: 'Mix Vegetable with Soy Sauce',
                  food_price: 'Rp 69.000,-',
                  food_img: Images.menu2,
                },
              ],
            },
          ],
        },
        {
          id: 3,
          category: 'Indian',
          img: Images.indian,
          resto: [
            {
              resto_id: 1,
              resto_name: 'Singh - Sector 50',
              est_time: '30 - 40 min',
              rating: '4.0',
              user_rating: 430,
              like: false,
              verify: false,
              bgHeader: Images.bgHeader,
              image: Images.bgAmerican,
              cusine: [{ name: 'Rice Delights' }, { name: 'Indian' }],
              menu: [
                {
                  food_id: 1,
                  food_name: 'Butter Chicken',
                  food_price: 'Rp 60.000,-',
                  food_img: Images.menu1,
                },
                {
                  food_id: 2,
                  food_name: 'Lamb Biryani',
                  food_price: 'Rp 69.000,-',
                  food_img: Images.menu2,
                },
                {
                  food_id: 3,
                  food_name: 'Yellow Daal',
                  food_price: 'Rp 79.000,-',
                  food_img: Images.menu3,
                },
              ],
            },
          ],
        },
        {
          id: 4,
          category: 'American',
          img: Images.american,
          resto: [
            {
              resto_id: 1,
              resto_name: 'Burger Singh Club - Sector 50',
              est_time: '30 - 40 min',
              rating: '4.3',
              user_rating: 530,
              like: true,
              verify: false,
              bgHeader: Images.bgHeader,
              image: Images.bgAmerican,
              cusine: [{ name: 'Burger' }, { name: 'American' }],
              menu: [
                {
                  food_id: 1,
                  food_name: 'Chicken Keema',
                  food_price: 'Rp 60.000,-',
                  food_img: Images.menu1,
                },
                {
                  food_id: 2,
                  food_name: 'Udta Punjab 2.0 Burger',
                  food_price: 'Rp 69.000,-',
                  food_img: Images.menu2,
                },
                {
                  food_id: 3,
                  food_name: 'Amnitsari Murgh Makhani',
                  food_price: 'Rp 79.000,-',
                  food_img: Images.menu3,
                },
                {
                  food_id: 4,
                  food_name: 'American Grilled',
                  food_price: 'Rp 50.000,-',
                  food_img: Images.menu2,
                },
              ],
            },
          ],
        },
      ],
      choose: [],
    }
  }

  onPress = ({ ...item }) => {
    this.setState({
      choose: item.resto,
    })
  }

  onPressNav = async (item) => {
    const { navigate } = await this.props.navigation
    navigate('details', { data: item })
  }

  render() {
    const { dataList, choose } = this.state
    return (
      <View style={Style.container}>
        <StatusBar hidden={true} />
        <View style={Style.header}>
          <TouchableOpacity style={Style.menu}>
            <Image source={Images.menu} style={Style.image} />
          </TouchableOpacity>
          <View style={Style.location}>
            <Image source={Images.pin} style={Style.imagePin} />
            <Text style={Style.text}>Sector 12, Noida</Text>
          </View>
          <TouchableOpacity style={Style.search}>
            <Image source={Images.search} style={Style.image} />
          </TouchableOpacity>
        </View>
        <FlatList
          data={dataList}
          extraData={dataList}
          keyExtractor={(item, key) => item + key}
          renderItem={({ item }) => <RenderHeader item={item} onPress={() => this.onPress(item)} />}
          horizontal={true}
          showsHorizontalScrollIndicator={false}
        />
        <View style={Style.listContainer}>
          {choose.length > 0 ? (
            <View style={Style.headerList}>
              <View style={Style.headerContainer}>
                <Text style={Style.title}>{choose.length} Restaurant Available</Text>
                <TouchableOpacity>
                  <Image source={Images.filter} style={Style.imageHeader} />
                </TouchableOpacity>
                <TouchableOpacity>
                  <Image source={Images.sort} style={Style.imageHeader} />
                </TouchableOpacity>
              </View>
              <FlatList
                data={choose}
                extraData={choose}
                keyExtractor={(item, key) => item + key}
                renderItem={({ item }) => (
                  <ListScreen item={item} onPressNav={() => this.onPressNav(item)} />
                )}
                showsVerticalScrollIndicator={false}
              />
            </View>
          ) : (
            <View style={Style.headerList}>
              <View style={Style.headerContainer}>
                <Text style={Style.title}>No Restaurant Available</Text>
                <TouchableOpacity>
                  <Image source={Images.filter} style={Style.imageHeader} />
                </TouchableOpacity>
                <TouchableOpacity>
                  <Image source={Images.sort} style={Style.imageHeader} />
                </TouchableOpacity>
              </View>
            </View>
          )}
        </View>
        <BottomNavigator />
      </View>
    )
  }
}

export default connect(
  null,
  null
)(MainScreen)

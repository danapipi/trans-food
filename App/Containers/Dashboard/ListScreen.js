/* eslint-disable react/prop-types */
import React from 'react'
import { Text, View, TouchableOpacity, Image } from 'react-native'
import { Images } from 'App/Theme'
import Style from './ListScreenStyle'

const ListScreen = ({ item, onPressNav }) => {
  return (
    <TouchableOpacity onPress={onPressNav}>
      <View style={Style.container}>
        <View style={Style.imgContainer}>
          <Image source={item.image} style={Style.bgImage} />
        </View>
        <View style={Style.contentContainer}>
          <View style={Style.titleContent}>
            <Text style={Style.title}>{item.resto_name}</Text>
            {item.like ? (
              <Image source={Images.like1} style={Style.imageLike} />
            ) : (
              <Image source={Images.like0} style={Style.imageLike} />
            )}
          </View>
          <View style={Style.key}>
            <Image source={Images.key} style={Style.imageKey} />
          </View>
          <View style={Style.bottomContent}>
            <View style={Style.bottomTextContent}>
              <Text style={Style.textContent}>Cuisine</Text>
              <View style={Style.containerCuisine}>
                {item.cusine.map((text, key) => (
                  <View key={key + 1} style={Style.cuisine}>
                    <Text key style={Style.textCuisine}>
                      {text.name}
                    </Text>
                  </View>
                ))}
              </View>
            </View>
            <View style={Style.bottomTextContentMid}>
              <Text style={Style.textContentMid}>Delivery Time</Text>
              <Text style={Style.estTime}>{item.est_time}</Text>
            </View>
            <View style={Style.bottomTextContent}>
              <Text style={Style.textContent}>Rating</Text>
              <View style={Style.ratingContainer}>
                <Text style={Style.textRating}>{item.rating}</Text>
                <Image source={Images.star} style={Style.imageRating} />
                {item.user_rating > 500 ? <Text>(500+)</Text> : <Text>(400+)</Text>}
              </View>
            </View>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  )
}

export default ListScreen

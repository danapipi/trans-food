import { StyleSheet } from 'react-native'
import Fonts from 'App/Theme/Fonts'
import { Colors } from 'App/Theme'

export default StyleSheet.create({
  container: {
    marginHorizontal: 10,
    marginBottom: 20,
  },
  title: {
    ...Fonts.style.normal,
    fontWeight: 'bold',
    marginHorizontal: 10,
    marginTop: 5,
  },
  image: {
    width: 20,
    height: 20,
  },
  imageLike: {
    width: 19,
    height: 15,
    marginRight: 19,
  },
  bgImage: {
    width: 320,
    height: 136,
    resizeMode: 'contain',
    flex: 1,
    zIndex: -10,
    position: 'absolute',
  },
  imgContainer: {
    elevation: 3,
    zIndex: -1,
    flex: 1,
    // position: 'absolute',
  },
  contentContainer: {
    borderBottomLeftRadius: 15,
    borderBottomRightRadius: 15,
    height: 120,
    width: 320,
    zIndex: 5,
    marginBottom: 5,
    marginTop: 130,
    elevation: 2,
    flex: 2,
  },
  titleContent: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical: 5,
  },
  textContent: {
    color: Colors.grey,
    ...Fonts.style.small,
    alignSelf: 'flex-start',
    marginLeft: 10,
  },
  textContentMid: {
    color: Colors.grey,
    ...Fonts.style.small,
    alignSelf: 'flex-start',
    marginLeft: 8,
  },
  textCuisine: {
    color: Colors.textRed,
    fontSize: 10,
    margin: 5,
  },
  containerCuisine: { flexDirection: 'row' },
  cuisine: { backgroundColor: Colors.bgRed, borderRadius: 15, marginHorizontal: 3 },
  key: {
    borderBottomColor: 'grey',
    borderBottomWidth: 0.5,
    marginHorizontal: 10,
  },
  imageKey: {
    width: 10,
    height: 10,
    marginBottom: 10,
  },
  bottomContent: {
    flexDirection: 'row',
  },
  bottomTextContent: {
    flexDirection: 'column',
    flex: 2,
    alignItems: 'center',
    marginTop: 10,
  },
  bottomTextContentMid: {
    flexDirection: 'column',
    flex: 1.5,
    alignItems: 'center',
    borderRightColor: 'grey',
    borderLeftColor: 'grey',
    borderLeftWidth: 0.3,
    borderRightWidth: 0.3,
    marginTop: 10,
  },
  estTime: {
    fontSize: 13,
    color: 'black',
    fontWeight: 'bold',
    marginTop: 3,
  },
  ratingContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 3,
  },
  imageRating: {
    width: 15,
    height: 15,
    marginHorizontal: 8,
  },
  textRating: {
    color: 'black',
    fontWeight: 'bold',
  },
})

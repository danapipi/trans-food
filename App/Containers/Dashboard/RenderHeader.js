import React from 'react'
import { Text, View, Image, TouchableOpacity } from 'react-native'
import Style from './RenderHeaderStyle'

// eslint-disable-next-line react/prop-types
const RenderHeader = ({ item, onPress }) => {
  return (
    <View style={Style.container}>
      <TouchableOpacity style={Style.button} onPress={onPress}>
        <Image source={item.img} style={Style.image} />
        <Text style={Style.title}>{item.category}</Text>
      </TouchableOpacity>
    </View>
  )
}

export default RenderHeader

import { StyleSheet } from 'react-native'
import Fonts from 'App/Theme/Fonts'
import ApplicationStyles from 'App/Theme/ApplicationStyles'
import { Colors } from 'App/Theme'

export default StyleSheet.create({
  container: {
    ...ApplicationStyles.screen.container,
    flex: 1,
    justifyContent: 'center',
    backgroundColor: Colors.primary,
  },
  title: {
    ...Fonts.style.h3,
    color: Colors.black,
    fontWeight: 'bold',
    marginLeft: 25,
    flex: 1,
  },
  listContainer: {
    ...ApplicationStyles.screen.listContainer,
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    backgroundColor: Colors.white,
  },
  image: {
    width: 20,
    height: 20,
  },
  imagePin: {
    width: 16,
    height: 21,
    marginHorizontal: 6,
    marginVertical: 5,
  },
  location: {
    backgroundColor: Colors.secondary,
    borderRadius: 15,
    flexDirection: 'row',
    paddingHorizontal: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    color: Colors.white,
    ...Fonts.style.small,
    marginVertical: 5,
  },
  header: {
    flexDirection: 'row',
    marginTop: 40,
    marginBottom: 30,
  },
  menu: {
    marginHorizontal: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  search: {
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 140,
  },
  headerList: {
    marginTop: 15,
    marginHorizontal: 10,
    marginBottom: 80,
  },
  imageHeader: {
    width: 60,
    height: 60,
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
})

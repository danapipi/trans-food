/**
 * Images should be stored in the `App/Images` directory and referenced using variables defined here.
 */

export default {
  allCat: require('App/Images/All_image.png'),
  american: require('App/Images/american_image.png'),
  chinese: require('App/Images/Chinese_image.png'),
  french: require('App/Images/French_image.png'),
  indian: require('App/Images/Indian_image.png'),
  menu: require('App/Images/Menu.png'),
  pin: require('App/Images/pin.png'),
  search: require('App/Images/Search.png'),
  filter: require('App/Images/Filter.png'),
  sort: require('App/Images/Sort.png'),
  bgAmerican: require('App/Images/BURGER2new.png'),
  bgChinese: require('App/Images/BURGER2new.png'),
  bgFrench: require('App/Images/BURGER2new.png'),
  bgIndian: require('App/Images/BURGER4.png'),
  like0: require('App/Images/Like2.png'),
  like1: require('App/Images/Like.png'),
  key: require('App/Images/key.png'),
  star: require('App/Images/STAR.png'),
  back: require('App/Images/Back.png'),
  bgHeader: require('App/Images/background_burger.png'),
  menu1: require('App/Images/burg1.png'),
  menu2: require('App//Images/burg2.png'),
  menu3: require('App/Images/burg3.png'),
  menu4: require('App/Images/burg4.png'),
}

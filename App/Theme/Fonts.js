const size = {
  h1: 32,
  h2: 24,
  h3: 18,
  h4: 16,
  regular: 17,
  medium: 14,
  small: 12,
}

const style = {
  h1: {
    fontSize: size.h1,
  },
  h2: {
    fontSize: size.h2,
  },
  h3: {
    fontSize: size.h3,
  },
  normal: {
    fontSize: size.regular,
  },
  medium: {
    fontSize: size.medium,
  },
  small: {
    fontSize: size.small,
  },
}

export default {
  size,
  style,
}

/**
 * This file contains the application's colors.
 *
 * Define color here instead of duplicating them throughout the components.
 * That allows to change them more easily later on.
 */

export default {
  transparent: 'rgba(0,0,0,0)',
  text: '#212529',
  primary: '#8d6cfb',
  secondary: '#7b5bd9',
  white: '#ffffff',
  black: '#000000',
  grey: '#d5d5d5',
  bgVerify: '#fc3440',
  textRed: '#ff9198',
  bgRed: '#fff1f2',
  buttonRed: '#ff424d',
}
